#include <Arduino.h>

#include "base64.hpp"
extern "C" {
#include <stdio.h>
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"

    int _gettimeofday(struct timeval *tv, struct timezone *tz) {
        tv->tv_sec = 0;
        tv->tv_usec = 0;
        return 0;
    }

}

extern "C" void serial_write(const char * buf, int len) {
    Serial.write(buf,len);
}
extern "C" void serial_print(const char * buf) {
    Serial.print(buf);
}

lua_State * L;

unsigned char buf_decode[4096] = {0};
void lua_exec(unsigned char * buf) {
    int n = decode_base64(buf, buf_decode);
    luaL_loadbuffer(L, buf_decode, n, "repl");
    Serial.println("");
    // int err = lua_pcall(L, 0, LUA_MULTRET, 0);
    int err = lua_pcall(L, 0, 0, 0);
    if (err) {
        Serial.print(lua_tolstring(L, 1, NULL));
        lua_pop(L, 1);
    }
}

void setup() {
    pinMode(13,OUTPUT); digitalWrite(13,HIGH);
    Serial.begin(9600);
    L = luaL_newstate();
    luaL_openlibs(L);
    digitalWrite(13,LOW);
}

char cmd[4096] = {0};
int ptr = 0;

void loop(){
    int c = -1;
    if (Serial.available() > 0) {
        c = Serial.read();
        cmd[ptr++] = c;
        if (ptr >= 4096) ptr = 0;
        if ((c == '\n') || (c == '\r')) {
            cmd[ptr++] = 0;
            lua_exec(cmd);
            ptr = 0;
        }
    }
}
