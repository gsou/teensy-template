#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <console/console.h>
#include <sys/printk.h>
#include <sys/__assert.h>
#include <usb/usb_device.h>
#include <drivers/uart.h>
#include <string.h>
#include <shell/shell.h>
#include "main.h"

#include <stdio.h>
#include "scheme-bind.h"
#include "scheme-private.h"
#include "scheme.h"
#include "lauxlib.h"
#include "lua.h"

/* size of stack area used by each thread */
#define STACKSIZE 1024

/* scheduling priority used by each thread */
#define PRIORITY 7

/* BUILD_ASSERT(DT_NODE_HAS_COMPAT(DT_CHOSEN(zephyr_console), zephyr_cdc_acm_uart), */
/* 	     "Console device is not ACM CDC UART device"); */

//#define SLEEP_TIME_MS 500
//#define LED0_NODE DT_ALIAS(led0)
//#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
//#define LED0 DT_GPIO_LABEL(LED0_NODE, gpios)
//#define PIN DT_GPIO_PIN(LED0_NODE, gpios)
//#define FLAGS DT_GPIO_FLAGS(LED0_NODE, gpios)
//#else
//#error "led0 devicetree alias not defined"
//#endif

//static void blink() {
//	const struct device * dev;
//	bool led_is_on = true;
//	int ret;
//
//	dev = device_get_binding(LED0);
//	ret = gpio_pin_configure(dev, PIN, GPIO_OUTPUT_ACTIVE | FLAGS);
//
//	while(1) {
//		gpio_pin_set(dev, PIN, (int) led_is_on);
//		led_is_on = ! led_is_on;
//		k_msleep(SLEEP_TIME_MS);
//	}
//}

scheme* sc;
lua_State * L;

char cmd[4096] = {0};
unsigned char buf_decode[4096] = {0};
int ptr = 0;

__attribute__((section(".text")))
#include "initscm.c"

void main(void)
{


	if (usb_enable(NULL)) {
		return;
	}

    /* k_sleep(K_MSEC(5000)); */

    sc = scheme_init_new();
    scheme_set_input_port_file(sc, stdin);
    scheme_set_output_port_file(sc, stdout);
    scheme_load_string(sc, init_scm);
    /* add_scheme_bindings(sc); */
    L = luaL_newstate();
	luaL_openlibs(L);
    fflush(stdout);

    printk("[INIT DONE]\n");
	k_sleep(K_MSEC(100));


	/* for(;;)k_sleep(K_MSEC(1000)); */
}

//K_THREAD_DEFINE(blink_id, STACKSIZE, blink, NULL, NULL, NULL,
//		PRIORITY, 0, 0);

void printkw(const char * buf) {
	printk(buf);
	k_sleep(K_MSEC(500));
}
