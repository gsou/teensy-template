#include <sys/times.h>
#include <unistd.h>

int open (const char * pathname, int flags, ...) {return -1;}
int _open (const char * pathname, int flags, ...) {return -1;}
int stat (const char * path, struct stat *sb) { return -1; }

clock_t _times(struct tms * buf) {
    int time = micros();
    buf->tms_utime = time;
    buf->tms_stime = time;
    buf->tms_cutime = time;
    buf->tms_cstime = time;
}

pid_t _getpid() {
  return 1;
}

pid_t _getppid() {
  return 1;
}

void * tmpfile() { return NULL; }

